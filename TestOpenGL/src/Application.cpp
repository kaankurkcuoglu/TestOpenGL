#include <iostream>;
#include <GL/glew.h>
#include <GLFW/glfw3.h>;
#include <fstream>;
#include <string>;
#include <sstream>;

using namespace std;

#define ASSERT(x) if(!(x)) __debugbreak();
#define GLCall(x) GLClearError(); \
 x;\
ASSERT(GLLogCall(#x, __FILE__, __LINE__))



static void GLClearError() 
{
	while (glGetError() != GL_NO_ERROR);
}


static bool GLLogCall(const char* function, const char* file, int line)
{
	while (GLenum error = glGetError())
	{
		cout << "OPENGL ERROR => (" << error << ")" << function <<
			" " << file << ":" << line << endl;
		return false;
	}

	return true;
}

enum class ShaderType
{
	NONE = -1, VERTEX= 0, FRAGMENT =1,
};

struct ShaderProgramSource
{

	string VertexSource;

	string FragmentSource;
};

static  ShaderProgramSource ParseShader(const string& filepath)
{
	ifstream stream(filepath);
	
	string line;
	stringstream ss[2];
	ShaderType type = ShaderType::NONE;

	int i = 0;

	while (getline(stream,line))
	{
		cout << line << endl;
		if (line.find("#shader") != string::npos)
		{
			
			if (line.find("vertex") != string::npos)
			{
				type = ShaderType::VERTEX;
			}
			if (line.find("fragment") != string::npos)
			{
				type = ShaderType::FRAGMENT;
			}

		}
		else
		{
			ss[(int)type] << line << '\n';
		}

	}

	return{ ss[0].str(), ss[1].str() };

}

static unsigned int CompileShader(unsigned int type, const string& source) {

	unsigned int id = glCreateShader(type);
	const char* src = source.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);

	int result;
	glGetShaderiv(id,GL_COMPILE_STATUS, &result);

	if (result == GL_FALSE) 
	{
		int lenght;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &lenght); 
		char* message = (char*) alloca(lenght * sizeof(char));
		glGetShaderInfoLog(id, lenght, &lenght, message);

		cout << "Failed to compilde shader" << 
			(type == GL_VERTEX_SHADER ? "vertex" : "fragment")<<endl;
		cout << message << endl;
		glDeleteShader(id);
		return 0;
	}


	return id;
}

static unsigned int CreateShader(const string& vertexShader, const string& fragmentShader)
{
	unsigned int program = glCreateProgram();
	unsigned int vs = CompileShader(GL_VERTEX_SHADER, vertexShader);
	unsigned int fs = CompileShader(GL_FRAGMENT_SHADER, fragmentShader);

	glAttachShader(program, vs);
	glAttachShader(program, fs);

	glLinkProgram(program);
	glValidateProgram(program);


	return program;

}

int main(void)
{
	GLFWwindow* window;

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);



	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);



	if (glewInit() != GLEW_OK)
		cout << "Glew Not OK!" << endl;

	cout << glGetString(GL_VERSION) << std::endl;


	float positions[] = {
		-0.5f, -0.5f,
		0.5f, -0.5f,
		0.5f, 0.5f,
		-0.5f, 0.5f,
	};


	unsigned int indicies[] = {
		0, 1, 2,
		2, 3, 0
	};

	unsigned int vao;
	GLCall(glGenVertexArrays(1, &vao));
	GLCall(glBindVertexArray(vao));

	unsigned int buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(float), positions, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, 0);

	unsigned int indexBuffer;
	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), indicies, GL_STATIC_DRAW);



	ShaderProgramSource source = ParseShader("res/shaders/Basic.shader");


	cout << "Vertex" << endl;
	cout << source.VertexSource << endl;
	cout << "Freagment" << endl;
	cout << source.FragmentSource << endl;


	unsigned int shader = CreateShader(source.VertexSource,source.FragmentSource);
	glUseProgram(shader);

	int location = glGetUniformLocation(shader, "u_Color");
	ASSERT(location != -1);
	glUniform4f(location, 1.0f, 0.0f, 0.5f, 1.0f);


	glBindVertexArray(0);
	glUseProgram(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);






	float b = 0.0f;
	float increment = 0.05f;

	glfwSwapInterval(20);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(shader);
		glUniform4f(location, 1.0f, b, 0.5f, 1.0f);


		glBindVertexArray(vao);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);






		GLCall(glDrawElements(GL_TRIANGLES,6 , GL_UNSIGNED_INT, nullptr));


		if (b > 1.0f || b < 0.0f)
		{
			increment = -increment;
		}

		b += increment;

		


		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}

	glDeleteProgram(shader);

	glfwTerminate();
	return 0;
}